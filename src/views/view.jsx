/// <reference path="tools/typings/react/react.d.ts" />

var view;
var Visibility = org.bitbucket.thofis.minesweeper_typescript.Visibility;
var MineState = org.bitbucket.thofis.minesweeper_typescript.MineState;
var FlagState = org.bitbucket.thofis.minesweeper_typescript.FlagState;

(function () {
    view.render = function (board) {
        React.render(
            <MineSweeper cols={board.getNrOfCols}
                         rows={board.getNrOfRows}
                         board={board}/>,
            document.getElementById('view')
        );
    };

    var Controls = React.createClass({
        render: function () {
            return (
                <div>
                    <h1>TODO: Controls...</h1>
                </div>
            );
        }
    });


    var Board = React.createClass({
        render: function () {
            return (
                <div id='board'>
                    <table>
                        {this.createRows()}
                    </table>
                </div>
            );
        },

        createRows: function () {
            var rows = [];
            for (var r = 0; r < this.props.rows; r++) {
                rows.push(<tr key={r}>{this.createCols(r)}</tr>);
            }
            return rows;
        },

        createCols: function (row) {
            var cols = [];
            for (var c = 0; c < this.props.cols; c++) {
                cols.push(<td key={c}><Cell board={this.props.board} cell={this.props.board.getCell(c,row)} col={c} row={row}/></td>)
            }
            return cols;
        }
    });

    var Cell = React.createClass({

        getInitialState: function () {
            return {
                clicked: false
            };
        },

        handleClick: function () {
            if (!this.state.clicked) {
                this.props.board.click(this.props.col,this.props.row);
                this.setState({clicked: true});
            }
            //console.log(this.props.cell.getMineState().toString());
        },

        handleRightClick: function(e) {
            e.preventDefault();
            this.props.board.toggleFlag(this.props.col,this.props.row);
        },

        getImageSource: function () {
            var imageName = '';
            var cell = this.props.cell;
            //console.log(cell.getMineState().toString())
            if (cell.getVisibility() === Visibility.HIDDEN) {
                if (cell.getFlagState() === FlagState.FLAGGED) {
                    imageName = 'flag';
                } else {
                    imageName = 'hidden';
                }
            } else {
                switch (cell.getMineState()) {
                    case MineState.ONE_NEIGHBOR:
                        imageName = '1';
                        break;
                    case MineState.TWO_NEIGHBORS:
                        imageName = '2';
                        break;
                    case MineState.THREE_NEIGHBORS:
                        imageName = '3';
                        break;
                    case MineState.FOUR_NEIGHBORS:
                        imageName = '4';
                        break;
                    case MineState.FIVE_NEIGHBORS:
                        imageName = '5';
                        break;
                    case MineState.SIX_NEIGHBORS:
                        imageName = '6';
                        break;
                    case MineState.SEVEN_NEIGHBORS:
                        imageName = '7';
                        break;
                    case MineState.EIGHT_NEIGHBORS:
                        imageName = '8';
                        break;
                    case MineState.MINE:
                        imageName = 'mine';
                        break;
                    default :
                        imageName = 'empty';
                        break;
                }
            }
            return 'images/' + imageName + '.png';
        },

        render: function () {
            var hiddenImage = "images/hidden.png";
            return (
                <img
                    className="cell"
                    src={this.state.clicked ? this.getImageSource() : hiddenImage}
                    onClick={this.handleClick}
                    onContextMenu={this.handleRightClick}
                    />
            );
        }

    });


    var MineSweeper = React.createClass({
        render: function () {
            return (
                <div>
                    <Controls/>
                    <Board board={this.props.board} cols={cols} rows={rows}/>
                </div>
            );
        }
    });
}(view || (view = {})));

