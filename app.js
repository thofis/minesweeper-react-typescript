var cols = 9;
var rows = 9;
var mines = 10;

var minesweeper = org.bitbucket.thofis.minesweeper_typescript;

window.onload = function () {
    var board = new minesweeper.Controller(cols, rows, mines, function (gameWon) {
        if (gameWon) {
            alert('Game won!');
        }
        else {
            alert('Game lost!');
        }
        gameOver(button);
    });
    view.render(board);
};